#!/usr/bin/env python3

STACK_INIT = 8
PORT_SIZE = 9

def readInitStack(f, port):
    initStack = [[] for _ in range(STACK_INIT)]

    for i in range(STACK_INIT):
        line = f.readline()
        for j in range(1, len(line), 4):
            if ord(line[j]) >= ord('A') and ord(line[j]) <= ord('Z'):
                initStack[i].append(line[j])
            else:
                initStack[i].append(' ')

    line = f.readline()
    line = f.readline()

    for i in range(STACK_INIT - 1, -1, -1):
        for j in range(PORT_SIZE):
            if initStack[i][j] != ' ':
                port[j].append(initStack[i][j])

def moveCrane(line, port):
        parts = line.split()

        for i in range(int(parts[1])):
            l = port[int(parts[3]) - 1].pop()
            port[int(parts[5]) - 1].append(l)


def main():
    with open("input.txt", "rt") as f:
        port = [[] for _ in range(PORT_SIZE)]
        readInitStack(f, port)
        line = f.readline()

        while line != "":
            moveCrane(line, port)
            line = f.readline()

        print("Heads of each stack: ", end='')

        for i in range(PORT_SIZE):
            print(port[i][-1], end='')

        print("")

if __name__ == "__main__":
    main()
