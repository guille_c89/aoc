# Day 5: Supply Stacks

[[_TOC_]]

## Part 1

The expedition can depart as soon as the final supplies have been unloaded from
the ships. Supplies are stored in stacks of marked crates, but because the
needed supplies are buried under many other crates, the crates need to be
rearranged.

The ship has a giant cargo crane capable of moving crates between stacks. To
ensure none of the crates get crushed or fall over, the crane operator will
rearrange them in a series of carefully-planned steps. After the crates are
rearranged, the desired crates will be at the top of each stack.

The Elves don't want to interrupt the crane operator during this delicate
procedure, but they forgot to ask her which crate will end up where, and they
want to be ready to unload them as soon as possible so they can embark.

They do, however, have a drawing of the starting stacks of crates and the
rearrangement procedure (your puzzle input). For example:

```text
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
```

In this example, there are three stacks of crates. Stack 1 contains two crates:
crate Z is on the bottom, and crate N is on top. Stack 2 contains three crates;
from bottom to top, they are crates M, C, and D. Finally, stack 3 contains a
single crate, P.

Then, the rearrangement procedure is given. In each step of the procedure, a
quantity of crates is moved from one stack to a different stack. In the first
step of the above rearrangement procedure, one crate is moved from stack 2 to
stack 1, resulting in this configuration:

```text
[D]        
[N] [C]    
[Z] [M] [P]
 1   2   3 
```

In the second step, three crates are moved from stack 1 to stack 3. Crates are
moved one at a time, so the first crate to be moved (D) ends up below the
second and third crates:

```text
        [Z]
        [N]
    [C] [D]
    [M] [P]
 1   2   3
```

Then, both crates are moved from stack 2 to stack 1. Again, because crates are
moved one at a time, crate C ends up below crate M:

```text
        [Z]
        [N]
[M]     [D]
[C]     [P]
 1   2   3
```

Finally, one crate is moved from stack 1 to stack 2:

```text
        [Z]
        [N]
        [D]
[C] [M] [P]
 1   2   3
```

The Elves just need to know which crate will end up on top of each stack; in
this example, the top crates are C in stack 1, M in stack 2, and Z in stack 3,
so you should combine these together and give the Elves the message CMZ.

After the rearrangement procedure completes, what crate ends up on top of each
stack?

### Refinement 1

Load initial location of cranes on stacks. Read instruction of stack's
movements, apply to cranes and print _head_ of each stack at end.

### Refinement 2

#### Data

##### Symbolic Constant: `PORT_SI`

Number of stacks (depends on input): `9`

##### Symbolic Constant: `STACK_IN`

Max high of initial stack of cranes (depends on input): `8`

##### Symbolic Constant: `LINE_SIZE`

Max line length on input: `37`

##### Structure: `Crane`

- `c`: crane's mark (a uppercase character).
- `n`: pointer to next crane.

#### Function: `newCrane`

Creates a new crane.

##### Inputs

- `l`: mark of the crane (uppercase character).

##### Output

Pointer to a new structure of type `Crane`.

##### Pseudocode

1. Invoke `malloc()` to RAM assignation to Crane pointer `cp`.
1. If RAM assignation is successful:
   1. Assign mark `l` to Crane pointer `cp`.
   1. Assign next crane as `NULL` to Crane pointer `cp`.
1. Else:
   1. Print error.
   1. End program execution.
1. Return `cp`.
#### Function: `pushCrane`

Add a crane to a stack.

##### Inputs

- `head`: double pointer to the _head_ of the stack.
- `l`: mark of the crane (uppercase character).

##### Output

Void (pointer changed to new head).

##### Pseudocode

1. Invoke `newCrane()` to mark `l`, assign return to Crane pointer `cp`.
1. If crane creation is successful (`cp` is different than `NULL`):
   1. Swap `head` to new crane `cp`.
1. Else:
   1. Print error.
   1. End program execution.
#### Function: `popCrane`

Remove the _head_ crane of a stack and return value.

##### Inputs

- `head`: double pointer to the _head_ of the stack.

##### Output

Mark of the crane (uppercase character).

##### Pseudocode

1. Declare character `l` and assign null `'\0'`.
1. If `head` is different than `NULL` (stack is not empty):
   1. Assign to `l` the mark of head's crane.
   1. Swap `head` to next Crane.
   1. Free old head.
1. Else:
   1. Print error.
1. Return `l`
#### Function: `readInitStack`

Read first lines of input an load initial stacks of cranes.

##### Inputs

- `fp`: pointer to file.
- `port`: array with stacks of cranes.

##### Output

Void.

##### Pseudocode

1. Declare matrix `initStack` of size `[STACK_IN][PORT_SI]` and assign zeros.
1. Declare string `line`.
1. For `i` equal `0`; `i` less than `STACK_IN`; increase `i`:
   1. Get a `line`.
   1. For `j` equal `1` and `k` equal `0`; `j` less than `LINE_SIZE`; increase
      `j` by `4`, `k` increase by `1`:
      1. If crane not empty:
         1. Assign mark from `line` to `initStack`.
      1. Else:
         1. Assign space `' '` to `initStack`.
1. For `i` equal `STACK_IN - 1`; `i` greater or equal than `0`; decrease `i`:
   1. For `j` equal `0`; `j` less than `PORT_SI`; increase `j`:
      1. If crane not empty:
         1. Invoke `pushCrane()` to `port[j]` and `initStack[i][j]`.
1. Get two lines.
#### Function: `moveCrane`

Read movements from file and apply on stacks.

##### Inputs

- `fp`: pointer to file.
- `port`: array with stacks of cranes.

##### Output

Void.

##### Pseudocode

1. Declare `cant`, `source` and `dest` to hold information from input file.
1. If information is read successfully from file:
   1. For `i` equal to `o`; until `i` less than `cant`; increment `i`:
      1. Invoke `popCrane()` for `source - 1` on `port` an save mark to `l`.
      1. Invoke `pushCrane()` for `dest - 1` on `port` and mark `l`.
1. Else:
   1. Print error.
   1. End program execution.

## Part Two 

As you watch the crane operator expertly rearrange the crates, you notice the
process isn't following your prediction.

Some mud was covering the writing on the side of the crane, and you quickly
wipe it away. The crane isn't a CrateMover 9000 - it's a CrateMover 9001.

The CrateMover 9001 is notable for many new and exciting features: air
conditioning, leather seats, an extra cup holder, and the ability to pick up
and move multiple crates at once.

Again considering the example above, the crates begin in the same
configuration:

```text
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 
 ```

Moving a single crate from stack 2 to stack 1 behaves the same as before:

```text
[D]        
[N] [C]    
[Z] [M] [P]
 1   2   3 
 ```

However, the action of moving three crates from stack 1 to stack 3 means that
those three moved crates stay in the same order, resulting in this new
configuration:

```text
        [D]
        [N]
    [C] [Z]
    [M] [P]
 1   2   3
```

Next, as both crates are moved from stack 2 to stack 1, they retain their order
as well:

```text
        [D]
        [N]
[C]     [Z]
[M]     [P]
 1   2   3
```

Finally, a single crate is still moved from stack 1 to stack 2, but now it's
crate C that gets moved:

```text
        [D]
        [N]
        [Z]
[M] [C] [P]
 1   2   3
```

In this example, the CrateMover 9001 has put the crates in a totally different
order: MCD.

Before the rearrangement process finishes, update your simulation so that the
Elves know where they should stand to be ready to unload the final supplies.
After the rearrangement procedure completes, what crate ends up on top of each
stack?

### Refinement

Same al part 1, only add a `temp` stack to save all movements on `moveCrane()`
and then apply to `dest`.
