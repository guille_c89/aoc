#include <stdio.h>
#include <stdlib.h>
#define PORT_SI 9
#define STACK_IN 8
#define LINE_SIZE 37

typedef struct {
	char c; // Crane name
	struct Crane *n; // Next crane
} Crane;

Crane *newCrane(char l);
void pushCrane(Crane ** head, char l);
char popCrane(Crane ** head);
void readInitStack(FILE *fp, Crane *port[]);
void moveCrane(Crane *port[], FILE *fp);

int main(void){
	Crane *port[PORT_SI] = {NULL};
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		readInitStack(fp, port);

		while (!feof(fp)){
			moveCrane(port, fp);
		}

		fclose(fp);

		printf("%s", "Heads of each stack: ");

		for (size_t i = 0; i < PORT_SI; i++){
			putc(port[i]->c, stdout);
		}

		puts("");

	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);

	}

	exit(EXIT_SUCCESS);
}

Crane *newCrane(char l){
	Crane *cp = malloc(sizeof(Crane));

	if (cp != NULL){
		cp->c = l;
		cp->n = NULL;
	}
	else {
		perror("newCrane");
		exit(EXIT_FAILURE);
	}

	return cp;
}

void pushCrane(Crane ** head, char l){
	Crane *cp = newCrane(l);

	if (cp != NULL){
		cp->n = (struct Crane *)*head;
		*head = cp;
	}
	else {
		perror("pushCrane");
		exit(EXIT_FAILURE);
	}
}

char popCrane(Crane ** head){
	char l = '\0';

	if (*head != NULL){
		l = (*head)->c;
		Crane *oldH = *head;
		*head = (Crane *)oldH->n;
		free(oldH);
	}
	else {
		fprintf(stderr, "%s", "popCrane: empty stack");
	}

	return l;
}

void readInitStack(FILE *fp, Crane *port[]){
	char initStack[STACK_IN][PORT_SI] = {0};
	char line[LINE_SIZE];

	for (size_t i = 0; i < STACK_IN; i++){
		fgets(line, LINE_SIZE, fp);
		for (size_t j = 1, k = 0; j < LINE_SIZE; j += 4, k++){
			if (line[j] >= 'A' && line[j] <= 'Z'){
				initStack[i][k] = line[j];
			}
			else {
				initStack[i][k] = ' ';
			}
		}
	}

	for (int i = STACK_IN - 1; i >= 0; i--){
		for (size_t j = 0; j < PORT_SI; j++){
			if (initStack[i][j] != ' '){
				pushCrane(&port[j], initStack[i][j]);
			}
		}
	}

	fgets(line, LINE_SIZE, fp);
	fgets(line, LINE_SIZE, fp);
}

void moveCrane(Crane *port[], FILE *fp){
	unsigned cant, source, dest;
	Crane *temp = NULL;

	if (fscanf(fp, "move %u from %u to %u\n", &cant, &source, &dest) == 3){
		for (size_t i = 0; i < cant; i++){
			char l = popCrane(&port[source - 1]);
			pushCrane(&temp, l);
		}
		while (temp != NULL){
			char l = popCrane(&temp);
			pushCrane(&port[dest - 1], l);
		}
	}
	else {
		fprintf(stderr, "%s", "fscanf: can't get input\n");
		exit(EXIT_FAILURE);
	}
}
