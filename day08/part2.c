#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define ROWS 99
#define COLS 99

void getForest(FILE *fp, unsigned forest[][COLS]);
unsigned movScore(size_t r, size_t c, unsigned forest[][COLS], int movR,
		int movC);
unsigned findScenic(unsigned forest[][COLS]);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned forest[ROWS][COLS];

		getForest(fp, forest);

		fclose(fp);

		printf("Better scenic score: %u\n", findScenic(forest));
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

void getForest(FILE *fp, unsigned forest[][COLS]){
	for (size_t i = 0; i < ROWS; i++){
		char line[COLS + 3];
		if (fgets(line, COLS + 3, fp) != NULL){
			for (size_t j = 0; j < COLS; j++){
				unsigned n = (unsigned) (line[j] - '0');

				if (n <= 9){
					forest[i][j] = n;
				}
				else {
					fprintf(stderr, "getForest: can't "
							"convert number %u.\n",
							n);
					exit(EXIT_FAILURE);
				}
			}
		}
		else {
			fprintf(stderr, "getForest: can't get line.\n");
			exit(EXIT_FAILURE);
		}
	}
}

unsigned movScore(size_t r, size_t c, unsigned forest[][COLS], int movR,
		int movC){
	int i = r + movR;
	int j = c + movC;
	unsigned score = 0;

	while (i > -1 && j > -1 && i < ROWS && j < COLS){
		score++;
		if (forest[i][j] >= forest[r][c]){
			break;
		}

		i += movR;
		j += movC;
	}

	return score;
}

unsigned findScenic(unsigned forest[][COLS]){
	unsigned better = 0;
	int movs[2][4] = {
		{-1, 0, 1,  0}, // Row movement
		{ 0, 1, 0, -1}  // Column movement
	};

	for (size_t i = 1; i < ROWS - 1; i++){
		for (size_t j = 1; j < COLS - 1; j++){
			unsigned score = 1;

			for (size_t k = 0; k < 4; k++){
				score *= movScore(i, j, forest, movs[0][k],
						movs[1][k]);
			}

			if (score > better){
				better = score;
			}
		}
	}

	return better;
}
