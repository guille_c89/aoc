# Day 8: Treetop Tree House

[[_TOC_]]

## Part 1

The expedition comes across a peculiar patch of tall trees all planted
carefully in a grid. The Elves explain that a previous expedition planted these
trees as a reforestation effort. Now, they're curious if this would be a good
location for a tree house.

First, determine whether there is enough tree cover here to keep a tree house
hidden. To do this, you need to count the number of trees that are visible from
outside the grid when looking directly along a row or column.

The Elves have already launched a quadcopter to generate a map with the height
of each tree (your puzzle input). For example:

```text
30373
25512
65332
33549
35390
```

Each tree is represented as a single digit whose value is its height, where 0
is the shortest and 9 is the tallest.

A tree is visible if all of the other trees between it and an edge of the grid
are shorter than it. Only consider trees in the same row or column; that is,
only look up, down, left, or right from any given tree.

All of the trees around the edge of the grid are visible - since they are
already on the edge, there are no trees to block the view. In this example,
that only leaves the interior nine trees to consider:

- The top-left 5 is visible from the left and top. (It isn't visible from the
  right or bottom since other trees of height 5 are in the way.)
- The top-middle 5 is visible from the top and right.
- The top-right 1 is not visible from any direction; for it to be visible,
  there would need to only be trees of height 0 between it and an edge.
- The left-middle 5 is visible, but only from the right.
- The center 3 is not visible from any direction; for it to be visible, there
  would need to be only trees of at most height 2 between it and an edge.
- The right-middle 3 is visible from the right.
- In the bottom row, the middle 5 is visible, but the 3 and 4 are not.

With 16 trees visible on the edge and another 5 visible in the interior, a
total of 21 trees are visible in this arrangement.

Consider your map; how many trees are visible from outside the grid?

### Refinement 1

Load matrix from file. For every inner tree (no edges) count if is visible form
edges. Print count.

### Refinement 2

#### Data

##### Symbolic Constant `ROWS`

Number of rows of matrix `forest`: 99

##### Symbolic Constant `COLS`

Number of columns of matrix `forest`: 99

#### Function `getForest`

Read a forest from file.

##### Input

- `fp`: pointer to file.
- `forest`: matrix of size `[ROWS][COLS]` of trees' heights.

##### Output

Void.

##### Pseudo-code
1. For `i` from `0`; until `i` less than `ROWS`; increment `i`:
   1. Declare char array `line` of size `COLS + 3`.
   1. Get line, if no error:
      1. For `j` from `0`; until `j` less than `COLS`; increment `j`:
         1. Declane `n` and assign the diference  between `line[j]` and `'0'`.
         1. If  `n` is less or equal to `9` (no error):
            1. Assign `n` to `forest[i][j]`.
         1. Else:
            1. Print error.
            1. Terminate program execution.
   1. Else:
      1. Print error.
      1. Terminate program execution.

#### Function `reachEdge`

True if a inner tree on row `r` and column `c` can reach the edge.

##### Input

- `r`: row of tree position.
- `c`: column of tree position.
- `forest`: matrix of size `[ROWS][COLS]` with tree's heights.
- `movR`: row movement (`-1` up, `0` still, `1` down).
- `movC`: col movement (`-1` left, `0` still, `1` right).

##### Output

True if movement can reach the edge through smaller trees, else false.

##### Pseudo-code

1. Declare `i` and assign `r` plus `movR`.
1. Declare `j` and assign `c` plus `movC`.
1. While `i` greater than `-1` and `j` greater than `-1` and `i` less than
   `ROWS` and `j` less than `COLS` (matrix boundaries):
   1. If tree `forest[i][j]` is greater or equal to tree `forest[r][c]`:
      1. Return false.
   1. Add `movR` to `i`.
   1. Add `movC` to `j`.
1. Return true.

#### Function `countVisible`

Returns number of trees visible from edge.

##### Input

- `forest`: matrix of size `[ROWS][COLS]` with tree's heights.

##### Output

Number of trees visible from edge.

##### Pseudo-code

1. Declare `count` and assign `0`.
1. Declare matrix `movs` of size `[2][4]` and assign directions for up/down and
   left/right.
1. For `i` from `1`; until `i` less than `ROWS - 1`; increment `i`:
   1. For `j` from `1`; until `j` less than `COLS - 1`; increment `j`:
      1. For `k` from `0`; until `k` less than `4`; increment `k`:
         1. If `reachEdge()` returns `true` for position `[i][j]`, movement
            `movs[][k]`:
            1. Increment count.
            1. Break inner for.
1. Return `count` plus `(COLS * 2) + ((ROWS - 2) * 2)`.

#### Function `main`

1. Open file.
1. If file opened successfully:
   1. Declare matrix `forest`  of size `[ROWS][COLS]`.
   1. For `i` from `0`; until `i` less than `ROWS`; increment `i`:
      1. Invoke `getRow()` for `fp` and `forest[i]`.
   1. Close file.
   1. Invoke `countVisible()` for `forest` and print return.
1. Else:
   1. Print error.
   1. Terminate program execution.

## Part Two

Content with the amount of tree cover available, the Elves just need to know
the best spot to build their tree house: they would like to be able to see a
lot of trees.

To measure the viewing distance from a given tree, look up, down, left, and
right from that tree; stop if you reach an edge or at the first tree that is
the same height or taller than the tree under consideration. (If a tree is
right on the edge, at least one of its viewing distances will be zero.)

The Elves don't care about distant trees taller than those found by the rules
above; the proposed tree house has large eaves to keep it dry, so they wouldn't
be able to see higher than the tree house anyway.

In the example above, consider the middle 5 in the second row:

```text
30373
25512
65332
33549
35390
```

- Looking up, its view is not blocked; it can see 1 tree (of height 3).
- Looking left, its view is blocked immediately; it can see only 1 tree (of
  height 5, right next to it).
- Looking right, its view is not blocked; it can see 2 trees.
- Looking down, its view is blocked eventually; it can see 2 trees (one of
  height 3, then the tree of height 5 that blocks its view).

A tree's scenic score is found by multiplying together its viewing distance in
each of the four directions. For this tree, this is 4 (found by multiplying 1 *
1 * 2 * 2).

However, you can do even better: consider the tree of height 5 in the middle of
the fourth row:

```text
30373
25512
65332
33549
35390
```

- Looking up, its view is blocked at 2 trees (by another tree with a height of
  5).
- Looking left, its view is not blocked; it can see 2 trees.
- Looking down, its view is also not blocked; it can see 1 tree.
- Looking right, its view is blocked at 2 trees (by a massive tree of height
  9).

This tree's scenic score is 8 (2 * 2 * 1 * 2); this is the ideal spot for the
tree house.

Consider each tree on your map. What is the highest scenic score possible for
any tree?

### Refinement 1

For every inner tree, calculate scenic score and find best view.

### Refinement 2

#### Data

##### Symbolic Constant `ROWS`

Number of rows of matrix `forest`: 99

##### Symbolic Constant `COLS`

Number of columns of matrix `forest`: 99

#### Function `getForest`

Read a forest from file.

##### Input

- `fp`: pointer to file.
- `forest`: matrix of size `[ROWS][COLS]` of trees' heights.

##### Output

Void.

##### Pseudo-code
1. For `i` from `0`; until `i` less than `ROWS`; increment `i`:
   1. Declare char array `line` of size `COLS + 3`.
   1. Get line, if no error:
      1. For `j` from `0`; until `j` less than `COLS`; increment `j`:
         1. Declane `n` and assign the diference  between `line[j]` and `'0'`.
         1. If  `n` is less or equal to `9` (no error):
            1. Assign `n` to `forest[i][j]`.
         1. Else:
            1. Print error.
            1. Terminate program execution.
   1. Else:
      1. Print error.
      1. Terminate program execution.

#### Function `movScore`

Count a scenic score of a inner tree on row `r` and column `c` with direction
`movR` and `movC`.

##### Input

- `r`: row of tree position.
- `c`: column of tree position.
- `forest`: matrix of size `[ROWS][COLS]` with tree's heights.
- `movR`: row movement (`-1` up, `0` still, `1` down).
- `movC`: col movement (`-1` left, `0` still, `1` right).

##### Output

Scenic score for tree on row `r` and column `c` with direction `movR` and
`movC`.

##### Pseudo-code

1. Declare `i` and assign `r` plus `movR`.
1. Declare `j` and assign `c` plus `movC`.
1. Declare `score` and assign `0`.
1. While `i` greater than `-1` and `j` greater than `-1` and `i` less than
   `ROWS` and `j` less than `COLS` (matrix boundaries):
   1. Increment `score`.
   1. If tree `forest[i][j]` is greater or equal to tree `forest[r][c]`:
      1. Break inner loop.
   1. Add `movR` to `i`.
   1. Add `movC` to `j`.
1. Return `score`.

#### Function `findScenic`

Returns scenic score of tree with better view.

##### Input

- `forest`: matrix of size `[ROWS][COLS]` with tree's heights.

##### Output

Scenic score of tree with better view.

##### Pseudo-code

1. Declare `better` and assign `0`.
1. Declare matrix `movs` of size `[2][4]` and assign directions for up/down and
   left/right.
1. For `i` from `1`; until `i` less than `ROWS - 1`; increment `i`:
   1. For `j` from `1`; until `j` less than `COLS - 1`; increment `j`:
      1. Declare `score` and assign `1`.
      1. For `k` from `0`; until `k` less than `4`; increment `k`:
	 1. Mutiply return  of `movScore()` to `score` for position `[i][j]`,
	    movement `movs[][k]`.
      1. If `score` is greater than `better`:
         1. Assign `score` to `better`. 
1. Return `better`.

#### Function `main`

1. Open file.
1. If file opened successfully:
   1. Declare matrix `forest`  of size `[ROWS][COLS]`.
   1. For `i` from `0`; until `i` less than `ROWS`; increment `i`:
      1. Invoke `getRow()` for `fp` and `forest[i]`.
   1. Close file.
   1. Invoke `countVisible()` for `forest` and print return.
1. Else:
   1. Print error.
   1. Terminate program execution.

