#!/usr/bin/env python3

ROWS = 99
COLS = 99

def getForest(f, forest):
    for i in range(ROWS):
        line = f.readline()
        for j in range(COLS):
            forest[i][j] = int(line[j])

def movScore(r, c, forest, movR, movC):
    i = r + movR
    j = c + movC
    score = 0

    while i > -1 and j > -1 and i < ROWS and j < COLS:
        score += 1
        if forest[i][j] >= forest[r][c]:
            break
        i += movR
        j += movC

    return score

def findScenic(forest):
    better = 0
    movs = [ [-1, 0, 1,  0], # Row movement
            [ 0, 1, 0, -1]]  # Column movement

    for i in range(1, ROWS - 1, 1):
        for j in range(1, COLS - 1, 1):
            score = 1

            for k in range(4):
                score *= movScore(i, j, forest, movs[0][k], movs[1][k])

            if score > better:
                better = score

    return better


def main():
    with open("input.txt", "rt") as f:
        forest = [[0 for _ in range(COLS)] for _ in range(ROWS)]
        getForest(f, forest)
        print("Better scenic score: " + str(findScenic(forest)))

if __name__ == "__main__":
    main()
