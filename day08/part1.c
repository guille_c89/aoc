#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define ROWS 99
#define COLS 99

void getForest(FILE *fp, unsigned forest[][COLS]);
bool reachEdge(size_t r, size_t c, unsigned forest[][COLS], int movR, int movC);
unsigned countVisible(unsigned forest[][COLS]);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned forest[ROWS][COLS];

		getForest(fp, forest);

		fclose(fp);

		printf("Visible trees: %u\n", countVisible(forest));
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

void getForest(FILE *fp, unsigned forest[][COLS]){
	for (size_t i = 0; i < ROWS; i++){
		char line[COLS + 3];
		if (fgets(line, COLS + 3, fp) != NULL){
			for (size_t j = 0; j < COLS; j++){
				unsigned n = (unsigned) (line[j] - '0');

				if (n <= 9){
					forest[i][j] = n;
				}
				else {
					fprintf(stderr, "getForest: can't "
							"convert number %u.\n",
							n);
					exit(EXIT_FAILURE);
				}
			}
		}
		else {
			fprintf(stderr, "getForest: can't get line.\n");
			exit(EXIT_FAILURE);
		}
	}
}

bool reachEdge(size_t r, size_t c, unsigned forest[][COLS], int movR, int movC){
	int i = r + movR;
	int j = c + movC;

	while (i > -1 && j > -1 && i < ROWS && j < COLS){
		if (forest[i][j] >= forest[r][c]){
			return false;
		}

		i += movR;
		j += movC;
	}

	return true;
}

unsigned countVisible(unsigned forest[][COLS]){
	unsigned count = 0;
	int movs[2][4] = {
		{-1, 0, 1,  0}, // Row movement
		{ 0, 1, 0, -1}  // Column movement
	};

	for (size_t i = 1; i < ROWS - 1; i++){
		for (size_t j = 1; j < COLS - 1; j++){
			for (size_t k = 0; k < 4; k++){
				if (reachEdge(i, j, forest, movs[0][k],
							movs[1][k])){
					count++;
					break;
				}
			}
		}
	}

	return count + (COLS * 2) + ((ROWS - 2) * 2);
}
