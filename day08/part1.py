#!/usr/bin/env python3

ROWS = 99
COLS = 99

def getForest(f, forest):
    for i in range(ROWS):
        line = f.readline()
        for j in range(COLS):
            forest[i][j] = int(line[j])

def reachEdge(r, c, forest, movR, movC):
    i = r + movR
    j = c + movC

    while i > -1 and j > -1 and i < ROWS and j < COLS:
        if forest[i][j] >= forest[r][c]:
            return False
        i += movR
        j += movC

    return True

def countVisible(forest):
    count = 0
    movs = [ [-1, 0, 1,  0], # Row movement
            [ 0, 1, 0, -1]]  # Column movement

    for i in range(1, ROWS - 1, 1):
        for j in range(1, COLS - 1, 1):
            for k in range(4):
                if reachEdge(i, j, forest, movs[0][k], movs[1][k]):
                    count += 1
                    break

    return count + (COLS * 2) + ((ROWS - 2) * 2)


def main():
    with open("input.txt", "rt") as f:
        forest = [[0 for _ in range(COLS)] for _ in range(ROWS)]
        getForest(f, forest)
        print("Visible trees: " + str(countVisible(forest)))

if __name__ == "__main__":
    main()
