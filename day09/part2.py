#!/usr/bin/env python3
from enum import Enum

R_L = 10
T = R_L - 1

class Direction(Enum):
	UP = 0
	DOWN = 1
	LEFT = 2
	RIGHT = 3

def moveP(p, d):
    if d == Direction.UP:
        return (p[0], p[1] + 1)
    elif d == Direction.DOWN:
        return (p[0], p[1] - 1)
    elif d == Direction.LEFT:
        return (p[0] - 1, p[1])
    elif d == Direction.RIGHT:
        return (p[0] + 1, p[1])

def getLine(line):
    parts = line.split()
    dirLetter = str(parts[0])
    steps = int(parts[1])

    if dirLetter == 'U':
        d = Direction.UP
    elif dirLetter == 'D':
        d = Direction.DOWN
    elif dirLetter == 'L':
        d = Direction.LEFT
    elif dirLetter == 'R':
        d = Direction.RIGHT
    else:
        print("Error: invalid direction input")
        exit()

    return d, steps

def calcT(h, t):
    if abs(h[0] - t[0]) == 2 and abs(h[1] - t[1]) == 2:
        return True, ((h[0] + t[0]) // 2, (h[1] + t[1]) // 2)
    elif abs(h[0] - t[0]) == 2:
        return True, ((h[0] + t[0]) // 2, h[1])
    elif abs(h[1] - t[1]) == 2:
        return True, (h[0],  (h[1] + t[1]) // 2)

    return False, t

def main():
    with open("input.txt", "rt") as f:
        r = [(0, 0) for i in range(R_L)]
        visited = [(0, 0)]
        count = 1
        line = f.readline()

        while line != "":
            d, steps = getLine(line)

            for i in range(steps):
                r[0] = moveP(r[0], d)
                for j in range(1, R_L):
                    moved, r[j] = calcT(r[j - 1], r[j])
                    if not moved:
                        break;
                    elif j == T:
                      if r[T] not in visited:
                          visited.append(r[T])
                          count += 1

            line = f.readline()

        print("Visited points: " + str(count))

if __name__ == "__main__":
    main()
