#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define R_L 10
#define T R_L - 1

typedef struct {
	int x;
	int y;
} Point;

typedef struct {
	Point p;
	struct List *n; // Next point
} List;

typedef enum {
	STILL = 0,
	UP,
	DOWN,
	LEFT,
	RIGHT
} Dir;

Point *newP(int x, int y);
void getLine(FILE *fp, Dir *dir, unsigned *steps);
void addP(List ** head, Point *cor);
bool searchP(List *head, Point *cor);
void clearList(List ** head);
void moveH(Point *p, Dir d);
bool calcT(Point *h, Point *t);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		Point r[R_L];
		List *visited = NULL;
		unsigned count = 1;
		Dir dir;
		unsigned steps;

		for (size_t i = 0; i < R_L; i++){
			r[i].x = 0;
			r[i].y = 0;
		}

		addP(&visited, &r[T]);

		while(!feof(fp)){
			getLine(fp, &dir, &steps);

			for (unsigned i = 0; i < steps; i++){
				moveH(&r[0], dir);
				for (size_t j = 1; j < R_L; j++){
					if (!calcT(&r[j - 1], &r[j])){
						break;
					}
					else if (j == T){
						if (!searchP(visited, &r[T])){
							addP(&visited, &r[T]);
							count++;
						}
					}
				}

			}
		}

		clearList(&visited);

		fclose(fp);

		printf("Visited points: %u\n", count);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

Point *newP(int x, int y){
	Point *p = malloc(sizeof(Point));

	if (p != NULL){
		p->x = x;
		p->y = y;
	}
	else {
		perror("newP");
		exit(EXIT_FAILURE);
	}

	return p;
}

void getLine(FILE *fp, Dir *dir, unsigned *steps){
	char dirLetter;

	if (fscanf(fp, "%c %u\n", &dirLetter, steps) != 2){
		fprintf(stderr, "Can't read line.\n");
		exit(EXIT_FAILURE);
	}

	switch (dirLetter){
		case 'U':
			*dir = UP;
			break;
		case 'D':
			*dir = DOWN;
			break;
		case 'L':
			*dir = LEFT;
			break;
		case 'R':
			*dir = RIGHT;
			break;
		default:
			fprintf(stderr, "No valid input direction.\n");
			exit(EXIT_FAILURE);
	}
}

void addP(List ** head, Point *cor){
	List *node = malloc(sizeof(List));

	if (node != NULL){
		node->p.x = cor->x;
		node->p.y = cor->y;
		node->n = (struct List *)*head;
		*head = node;
	}
	else {
		perror("addP");
		exit(EXIT_FAILURE);
	}
}

bool searchP(List *head, Point *cor){
	List *node = head;

	while (node != NULL){
		if (node->p.x == cor->x && node->p.y == cor->y){
			return true;
		}
		node = (List *)node->n;
	}

	return false;
}

void clearList(List ** head){
	List *node = *head;

	while (node != NULL){
		node = (List *)(*head)->n;
		free(*head);
		*head = node;
	}
}

void moveH(Point *p, Dir d){
	switch (d){
		case UP: (p->y)++; break;
		case DOWN: (p->y)--; break;
		case LEFT: (p->x)--; break;
		case RIGHT: (p->x)++; break;
		case STILL: break;
	}
}

// Taken from: https://github.com/anthonywritescode/aoc2022/blob/main/day09/part2.py#L21
bool calcT(Point *h, Point *t){
	bool moved = false;

	if (abs(h->x - t->x) == 2 && abs(h->y - t->y) == 2){
		t->x = (h->x + t->x) / 2;
		t->y = (h->y + t->y) / 2;
		moved = true;
	}
	else if (abs(h->x - t->x) == 2){
		t->x = (t->x + h->x) / 2;
		t->y = h->y;
		moved = true;
	}
	else if (abs(h->y - t->y) == 2){
		t->x = h->x;
		t->y = (t->y + h->y) / 2;
		moved = true;
	}

	return moved;
}
