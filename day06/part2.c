#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define LENS_SIZE 14

bool isSet(char a[], size_t n);
unsigned findSet(FILE *fp);
void insC(char a[], size_t n, char c);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		printf("Number of character: %u\n", findSet(fp));
		fclose(fp);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

bool isSet(char a[], size_t n){
	for (size_t i = 0; i < n; i++){
		for (size_t j = 0; j < n; j++){
			if (i != j && a[i] == a[j]){
				return false;
			}
		}
	}

	return true;
}

void insC(char a[], size_t n, char c){
	for (size_t i = 0; i < n - 1; i++){
		a[i] = a[i + 1];
	}

	a[n - 1] = c;
}

unsigned findSet(FILE *fp){
	char lens[LENS_SIZE];
	unsigned count = LENS_SIZE;

	for (size_t i = 0; i < LENS_SIZE; i++){
		if (fscanf(fp, " %c", &lens[i]) != 1){
			fprintf(stderr, "%s", "fscanf: can't get input");
			exit(EXIT_FAILURE);
		}
	}

	while (!isSet(lens, LENS_SIZE) && !feof(fp)){
		char c;

		if (fscanf(fp, " %c", &c) != 1){
			fprintf(stderr, "%s", "fscanf: can't get input");
			exit(EXIT_FAILURE);
		}
		else {
			insC(lens, LENS_SIZE, c);
			count++;
		}
	}

	return count;
}
