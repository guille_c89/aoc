#!/usr/bin/env python3

LENS_SIZE = 4

def isSet(lens):
    size = len(lens)
    for i in range(size):
        for j in range(size):
            if i != j and lens[i] == lens[j]:
                return False
    return True

def findSet(f, size):
    lens = []
    count = size
    
    for i in range(size):
        lens.append(f.read(1))

    c = f.read(1)
    while isSet(lens) == False and c != '':
        lens.pop(0)
        lens.append(c)
        count += 1
        c = f.read(1)

    return count

def main():
    with open("input.txt", "rt") as f:
        print("Number of character: " + str(findSet(f, LENS_SIZE)))


if __name__ == "__main__":
    main()
