#!/usr/bin/env python3

def main():
    with open("input.txt", "rt") as f:
        x = 1
        cycle = 1
        result = 0
        line = f.readline()
        while line != "":
            dur = 1
            lparts = line.split()
            isnt = str(lparts[0])
            if isnt == "addx":
                v = int(lparts[1])
                dur = 2
            for _ in range(dur):
                if (cycle + 20) % 40 == 0:
                    result += (x * cycle)
                cycle += 1
            if isnt == "addx":
                x += v
            line = f.readline()
    print("Signal strenght: "+str(result))

if __name__ == "__main__":
    main()
