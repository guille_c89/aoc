#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define W_SIZE 5

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		char inst[W_SIZE];
		int x = 1;
		unsigned cycle = 1;
		int result = 0;

		while(!feof(fp)){
			unsigned dur = 1;
			int v;

			fscanf(fp, "%4s", inst);

			if (strcmp(inst, "addx") == 0){
				fscanf(fp, "%d", &v);
				dur = 2;
			}

			for (; dur > 0; dur--, cycle++){
				if ((cycle + 20) % 40 == 0){
					result += x * cycle;
				}
			}

			if (strcmp(inst, "addx") == 0){
				x += v;
			}
		}

		fclose(fp);

		printf("Sum of signal strenghts: %d\n", result);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
}
