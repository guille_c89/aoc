#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define W_SIZE 5

void printBeam(unsigned c, int x);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		char inst[W_SIZE];
		int x = 1;
		unsigned cycle = 1;

		while(!feof(fp)){
			unsigned dur = 1;
			int v;

			fscanf(fp, "%4s", inst);

			if (strcmp(inst, "addx") == 0){
				fscanf(fp, "%d", &v);
				dur = 2;
			}

			for (; dur > 0; dur--, cycle++){
				printBeam(cycle, x);
			}

			if (strcmp(inst, "addx") == 0){
				x += v;
			}
		}

		fclose(fp);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
}

void printBeam(unsigned c, int x){
	int pos = (c - 1) % 40;

	if (x - 1 == pos || x == pos || x + 1 == pos){
		putc('#', stdout);
	}
	else {
		putc('.', stdout);
	}
	
	if (pos == 39) putc('\n', stdout);
}
