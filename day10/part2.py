#!/usr/bin/env python3

def printBeam(c, x):
    pos = (c - 1) % 40

    if x - 1 == pos or x == pos or x + 1 == pos:
        print('#', end='')
    else: 
        print('.', end='')

    if pos == 39:
        print('')

def main():
    with open("input.txt", "rt") as f:
        x = 1
        cycle = 1
        line = f.readline()
        while line != "":
            dur = 1
            lparts = line.split()
            isnt = str(lparts[0])
            if isnt == "addx":
                v = int(lparts[1])
                dur = 2
            for _ in range(dur):
                printBeam(cycle, x)
                cycle += 1
            if isnt == "addx":
                x += v
            line = f.readline()

if __name__ == "__main__":
    main()
