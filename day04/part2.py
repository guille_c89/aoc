#!/usr/bin/env python3

class Sections:
    def __init__(self, low, high):
        self.l = low
        self.h = high

    def isOverlap(self, e2):
        if self.h >= e2.l and self.l <= e2.h:
            return True
        return False

def main():
    with open("input.txt", "rt") as f:
        count = 0
        line = f.readline()
        while line != "":
           els = []
           pairs = line.split(",")

           for pair in pairs:
               val = pair.split("-")
               els.append(Sections(int(val[0]), int(val[1])))

           if els[0].isOverlap(els[1]):
               count += 1

           line = f.readline()

        print("Sum of contained sections: " + str(count))

if __name__ == "__main__":
    main()
