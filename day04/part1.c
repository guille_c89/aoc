#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
	unsigned l;
	unsigned h;
} Sections;

bool isEncap(Sections e1, Sections e2);

int main(void){
	FILE *fp = fopen("input.txt", "r");
	unsigned count = 0;

	if (fp != NULL){
		while(!feof(fp)){
			Sections els[2];
			fscanf(fp, "%u-%u,%u-%u\n",
					&els[0].l, &els[0].h,
					&els[1].l, &els[1].h);

			if (isEncap(els[0], els[1])) count++;
			else if (isEncap(els[1], els[0])) count++;
		}

		fclose(fp);
		
		printf("Sum of contained sections: %u\n", count);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

bool isEncap(Sections e1, Sections e2){
	if (e1.l >= e2.l && e1.h <= e2.h){
		return true;
	}

	return false;
}
