#include <stdio.h>
#include <stdlib.h>

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned points = 0;
		char chosen, oppo;

		while (!feof(fp)){
			fscanf(fp, "%c %c\n", &oppo, &chosen);
			unsigned chPos = chosen - 'X';
			unsigned opPos = oppo - 'A';

			points += chPos + 1;

			if (chPos == opPos){
				points += 3;
			}
			else if ((chPos + 2) % 3 == opPos){
				points += 6;
			}
		}

		printf("Total points: %u\n", points);

		fclose(fp);
	}
	else {
		perror("fopen");
	}

	exit(EXIT_SUCCESS);
}
