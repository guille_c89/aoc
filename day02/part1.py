#!/usr/bin/env python3

with open("input.txt", "rt") as f:
    line = f.readline()
    points = 0
    while line != "":
        match = line.split()
        chPos = ord(match[1]) - ord('X')
        opPos = ord(match[0]) - ord('A')

        points += chPos + 1

        if chPos == opPos:
            points += 3
        elif (chPos + 2) % 3 == opPos:
            points += 6

        line = f.readline()
    print("Total points: " + str(points))
