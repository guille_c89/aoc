#include <stdio.h>
#include <stdlib.h>

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned points = 0;
		char outc, oppo;

		while (!feof(fp)){
			fscanf(fp, "%c %c\n", &oppo, &outc);
			unsigned opPos = oppo - 'A';

			if (outc == 'X'){
				points += ((opPos + 2) % 3) + 1;
			}
			else if (outc == 'Y'){
				points += 3;
				points += opPos + 1;
			}
			else if (outc == 'Z'){
				points += 6;
				points += ((opPos + 1) % 3) + 1;
			}
		}

		printf("Total points: %u\n", points);

		fclose(fp);
	}
	else {
		perror("fopen");
	}

	exit(EXIT_SUCCESS);
}
