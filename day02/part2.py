#!/usr/bin/env python3

with open("input.txt", "rt") as f:
    line = f.readline()
    points = 0
    while line != "":
        match = line.split()
        opPos = (ord(match[0]) - ord('A'))

        if match[1] == 'X':
            points += ((opPos + 2) % 3) + 1
        elif match[1] == 'Y':
            points += 3
            points += opPos + 1
        elif match[1] == 'Z':
            points += 6
            points += ((opPos + 1) % 3) + 1

        line = f.readline()
    print("Total points: " + str(points))
