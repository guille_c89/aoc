#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Vertex {
	bool out;
	int dist;
	char level;
} Vertex;

typedef struct Coord {
	size_t r;
	size_t c;
} Coord;

Vertex *readInput(Coord *start, Coord *dim, char *filepath);
size_t dijkstra(Coord *dim, Vertex *map);

int main(int argc, char *argv[]){
	if (argc == 2){
		Coord start, dim;
		Vertex *map = readInput(&start, &dim, argv[1]);

		map[start.r * dim.c + start.c].dist = 0;
		map[start.r * dim.c + start.c].out = true;
		map[start.r * dim.c + start.c].level = 'a';

		printf("Short distance: %zu\n", dijkstra(&dim, map));

		free(map);
	}
	else {
		printf("%s", "Must suply one file path.\n");
	}

	return 0;
}

Vertex *readInput(Coord *start, Coord *dim, char *filepath){
	FILE *fp = fopen(filepath, "r");
	Vertex *map = NULL;

	if (fp != NULL){
		char c;
		dim->r = 0;
		dim->c = 0;

		while (!feof(fp) && (c = fgetc(fp)) != '\n') (dim->c)++;

		do {
			if (c == '\n') (dim->r)++;
		} while (!feof(fp) && (c = fgetc(fp))) ;

		unsigned arrSize =(sizeof(Vertex) * (dim->r * dim->c));
		map = (Vertex *)malloc(arrSize);

		if (map != NULL){
			unsigned i = 0;

			fseek(fp, 0, SEEK_SET);
			c = fgetc(fp);

			while (!feof(fp)){

				if (c != '\n'){
					if (c == 'S'){
						start->r = i / dim->c;
						start->c = i % dim->c;
					}

					map[i].level = c;
					map[i].dist = -1;
					map[i++].out = false;

				}

				c = fgetc(fp);
			}

			fclose(fp);
		}
		else {
			perror("map_malloc");
			exit(EXIT_FAILURE);
		}
	}
	else {
		perror("readInput");
		exit(EXIT_FAILURE);
	}

	return map;
}

size_t dijkstra(Coord *dim, Vertex *map){
	bool end = false;
	size_t dist = 0;

//	while (!end){
//		for (size_t i = 0; i < dim->r; i++){
//			for (size_t j = 0; j < dim->c; j++){
//			}
//		}
//	}

	return dist;
}

bool analyseVertex(Coord *dim, Coord *node, Vertex *map){
	bool end = false;
	size_t n = node->r * dim->c + node->c;
	bool isOut = true;
	int dirs[2][4] = {
		{-1, 0, 0, 1},
		{ 0, -1, 1, 0}
	};

	if (!(map[n].out) && map[n].dist >= 0){
		for (size_t i = 0; i < 4; i++){
			size_t n = node->r * dim->c + node->c;
		}
	}

	return end;
}
