#!/usr/bin/env python3

def findItem(line, n):
    for i in range(int(n / 2)):
        if line[i] in line[int(n / 2):]:
            return line[i]

def calcPrior(c):
    if c.islower():
        return ord(c) - ord('a') + 1
    else:
        return ord(c) - ord('A') + 27

def main():
    with open("input.txt", "rt") as f:
        total = 0
        line = f.readline()
        while line != "":
            lenLn = len(line)
            total += calcPrior(findItem(line, lenLn))
            line = f.readline()
        print("Sum of priorities: " + str(total))

if __name__ == "__main__":
    main()
