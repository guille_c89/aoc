#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX_LINE 50

char findItem(const char line[], size_t n);
unsigned calcPrior(char c);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		char line[MAX_LINE];
		unsigned total = 0;

		fgets(line, MAX_LINE, fp);

		while(!feof(fp)){
			size_t lenLn = strlen(line);

			total += calcPrior(findItem(line, lenLn));

			fgets(line, MAX_LINE, fp);
		}

		fclose(fp);

		printf("Sum of priorities: %u\n", total); 
	}
	else {
		perror("fopen");
	}

	exit(EXIT_SUCCESS);
}

char findItem(const char line[], size_t n){
	for (size_t i = 0; i < n / 2; i++){
		for (size_t j = n / 2; j < n; j++){
			if (line[i] == line[j]){
				return line[i];
			}
		}
	}

	fprintf(stderr, "%s", "Error: duplicated item not found.");
	exit(EXIT_FAILURE);
}

unsigned calcPrior(char c){
	if (islower(c)){
		return c - 'a' + 1;
	}
	else {
		return c - 'A' + 27;
	}
}
