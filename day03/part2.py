#!/usr/bin/env python3

GROUP_SIZE = 3

def readLines(lines, f):
    for i in range(GROUP_SIZE):
        lines[i] = f.readline()

def findItem(lines):
    for c in lines[0]:
            present = True

            for l in lines[1:]:
                if c not in l:
                    present = False
                    break

            if present:
                return c

    print("Error: common item not found")
    return '\0'

def calcPrior(c):
    if c.islower():
        return ord(c) - ord('a') + 1
    else:
        return ord(c) - ord('A') + 27

def main():
    with open("input.txt", "rt") as f:
        total = 0
        lines = [["INIT"] for _ in range(GROUP_SIZE)]
        readLines(lines, f)

        while lines[0] != "":
            total += calcPrior(findItem(lines))
            readLines(lines, f)

        print("Sum of priorities: " + str(total))

if __name__ == "__main__":
    main()
