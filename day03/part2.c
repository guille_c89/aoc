#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#define GROUP_SIZE 3
#define MAX_LINE 50

void readLines(char lines[GROUP_SIZE][MAX_LINE], FILE *fp);
bool cInLine(char c, char line[], size_t n);
char findItem(char lines[GROUP_SIZE][MAX_LINE]);
unsigned calcPrior(char c);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		char lines[GROUP_SIZE][MAX_LINE];
		unsigned total = 0;

		readLines(lines, fp);

		while(!feof(fp)){
			total += calcPrior(findItem(lines));
			readLines(lines, fp);
		}

		fclose(fp);

		printf("Sum of priorities: %u\n", total); 
	}
	else {
		perror("fopen");
	}

	exit(EXIT_SUCCESS);
}

void readLines(char lines[GROUP_SIZE][MAX_LINE], FILE *fp){
		for (size_t i = 0; i < GROUP_SIZE; i++){
			fgets(lines[i], MAX_LINE, fp);
		}
}

bool cInLine(char c, char line[], size_t n){
	for (size_t i = 0; i < n; i++){
		if (c == line[i]){
			return true;
		}
	}
	return false;
}

char findItem(char lines[GROUP_SIZE][MAX_LINE]){
	size_t nLs[GROUP_SIZE];

	for (size_t i = 0; i < GROUP_SIZE; i++){
		nLs[i] = strlen(lines[i]);
	}

	for (size_t i = 0; i < nLs[0]; i++){
		bool present = true;

		for (size_t j = 1; present && j < GROUP_SIZE; j++){
			if (!cInLine(lines[0][i], lines[j], nLs[j])){
				present = false;
			}
		}

		if (present){
			return lines[0][i];
		}
	}

	fprintf(stderr, "%s", "Error: common item not found.");
	exit(EXIT_FAILURE);
}

unsigned calcPrior(char c){
	if (islower(c)){
		return c - 'a' + 1;
	}
	else {
		return c - 'A' + 27;
	}
}
