#!/usr/bin/env python3

POD_SIZE = 3

with open("input.txt", 'rt') as f:
    count = 0
    podium = []
    for i in range(POD_SIZE):
        podium.append(0)

    line = f.readline()

    while line != "":
        if line != "\n":
            count += int(line)
        else:
            for i in range(POD_SIZE):
                if podium[i] <= count:
                    for j in range(POD_SIZE - 1, i, -1):
                        podium[j] = podium[j - 1]
                    podium[i] = count
                    break
            count = 0
        line = f.readline()

    print("Top three calories: " + str(sum(podium)))
