#!/usr/bin/env python3

with open("input.txt", 'rt') as f:
    podium = []
    count = 0
    line = f.readline()

    while line != "":
        if line != "\n":
            count += int(line)
        else:
            podium.append(count)
            count = 0
        line = f.readline()

    podium.sort()
    print("Top three calories: " + str(sum(podium[-3:])))
