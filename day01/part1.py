#!/usr/bin/env python3

with open("input.txt", 'rt') as f:
    maxCal = 0
    count = 0
    line = f.readline()

    while line != "":
        if line != "\n":
            count = count + int(line)
        else:
            if count > maxCal:
                maxCal = count
            count = 0
        line = f.readline()

    print("Maximun calories: "+str(maxCal))
