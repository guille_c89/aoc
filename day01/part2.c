#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#define MAX_LINE 24
#define POD_SIZE 3

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned long podium[POD_SIZE] = {0UL};
		unsigned long count = 0UL;

		char line[MAX_LINE];
		fgets(line, MAX_LINE, fp);

		while(!feof(fp)){
			if (strcmp(line, "\n")){
				errno = 0;
				unsigned long num = strtoul(line, NULL, 10);

				if (errno != 0) {
					perror("strtoul");
					fclose(fp);
					exit(EXIT_FAILURE);
				}

				if (num > 0 && count > ULONG_MAX - num) {
					fprintf(stderr, "%s", "Error: addition"
							" overflow\n");
					fclose(fp);
					exit(EXIT_FAILURE);
				}

				count += num;
			}
			else {
				for (size_t i = 0; i < POD_SIZE; i++){
					if (podium[i] <= count){
						for (size_t j = POD_SIZE - 1;
								j > i; j--){
							podium[j] = podium[j - 1];
						}
						podium[i] = count;
						break;
					}
				}
				count = 0UL;
			}

			fgets(line, MAX_LINE, fp);
		}

		fclose(fp);

		unsigned long sum = 0;
		for (size_t i = 0; i < POD_SIZE; i++) sum += podium[i];
		printf("Top three provisions: %lu\n", sum);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
