#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#define MAX_LINE 32

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		unsigned long max = 0UL;
		unsigned long count = 0UL;

		char line[MAX_LINE];
		fgets(line, MAX_LINE, fp);

		while(!feof(fp)){
			if (strcmp(line, "\n")){
				errno = 0;
				unsigned long num = strtoul(line, NULL, 10);

				if (errno != 0) {
					perror("strtoul");
					fclose(fp);
					exit(EXIT_FAILURE);
				}

				if (num > 0 && count > ULONG_MAX - num) {
					fprintf(stderr, "%s", "Error: addition"
							" overflow\n");
					fclose(fp);
					exit(EXIT_FAILURE);
				}

				count += num;
			}
			else {
				if (count > max) max = count;
				count = 0UL;
			}

			fgets(line, MAX_LINE, fp);
		}

		fclose(fp);
		printf("Maximun calories: %lu\n", max);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
