#!/usr/bin/env python3

class Monkey:
    def __init__(self, num, items, op, op_num, test_num, if_true, if_false):
        self.id = num
        self.items = items
        self.op = op
        self.op_num = op_num
        self.test_num = test_num
        self.if_true = if_true
        self.if_false = if_false
        self.inspecs = 0
        
    def __gt__(self, value):
        return self.inspecs > value.inspecs

    def addItem(self, i):
        self.items.append(i)

    def getItem(self):
        return self.items.pop(0)

    def inspectItem(self):
        if self.op == '+':
            self.items[0] += self.op_num
        elif self.op == '*' and self.op_num != 0:
            self.items[0] *= self.op_num
        else:
            self.items[0] *= self.items[0]

        self.items[0] = int(self.items[0] // 3)
        self.inspecs += 1

        if self.items[0] % self.test_num == 0:
            return self.if_true
        else:
            return self.if_false

def readInput():
    monkeys = []
    with open("input.txt", "rt") as f:
        line = f.readline()

        while (line != ""):
            num = int(line.split()[-1].split(':')[0])

            items = list()
            for i in f.readline().split(':')[-1].split(','):
                items.append(int(i))

            line = f.readline().split()
            op = str(line[-2])
            if line[-1] != "old":
                op_num = int(line[-1])
            else:
                op_num = 0

            test_num = int(f.readline().split()[-1])
            if_true = int(f.readline().split()[-1])
            if_false = int(f.readline().split()[-1])

            monkeys.append(Monkey(num, items, op, op_num, test_num, if_true,
                                  if_false))

            line = f.readline()
            line = f.readline()

    return monkeys

def main():
    monkeys = readInput()

    for r in range(20):
        for m in monkeys:
            for i in range(len(m.items)):
                t = m.inspectItem()
                monkeys[t].addItem(m.getItem())
    
    monkeys.sort(reverse=True)
    print("Result: " + str(monkeys[0].inspecs * monkeys[1].inspecs))

if __name__ == "__main__":
    main()
