#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#define MAX_LINE 80

typedef struct Item {
	long unsigned worry;
	struct Item *next;
} Item;

typedef struct {
	Item *start;
	Item *end;
} ItemBuffer;

typedef struct Monkey {
	unsigned id;
	ItemBuffer items;
	char op;
	unsigned op_num;
	unsigned test_num;
	unsigned if_true;
	unsigned if_false;
	unsigned inspecs;
	struct Monkey * next;
} Monkey;

Item *createItem(long unsigned w);
void addToItems(ItemBuffer *ib, Item *n);
Item *removeFromItems(ItemBuffer *ib);
void freeItems(Item *s);
Monkey *createMonkey(unsigned id, ItemBuffer items, char op, unsigned op_num,
		unsigned test_num, unsigned if_true, unsigned if_false);
Monkey *addToMonkeys(Monkey *s, Monkey *n);
void freeMonkeys(Monkey *s);
void getMonkeyNum(FILE *fp, char *sf, unsigned *n);
void getMonkeyItems(FILE *fp, ItemBuffer *ib);
void getMonkeyOp(FILE *fp, char *m_op, unsigned *m_op_num);
Monkey *readInput(void);
void monkeyTurn(Monkey *s, Monkey *m, unsigned cDiv);
unsigned calcWorry(char op, unsigned op_num, long unsigned i, unsigned cDiv);
unsigned calcThrow(unsigned test, unsigned if_true, unsigned if_false,
		long unsigned i);
void throwItem(Monkey *s, unsigned id, Item *i);
Monkey *monkeySortIns(Monkey *s);

Item *createItem(long unsigned w){
	Item *i = (Item *)malloc(sizeof(Item));

	if ( i != NULL) {
		i->worry = w;
		i->next = NULL;
	}
	else {
		perror("createItem");
		exit(EXIT_FAILURE);
	}

	return i;
}

void addToItems(ItemBuffer *ib, Item *n){
	n->next = NULL;

	if (ib->end != NULL){
		ib->end->next = n;
		ib->end = n;
	}
	else {
		ib->start = ib->end = n;
	}
}

Item *removeFromItems(ItemBuffer *ib){
	Item *n = NULL;

	if (ib->start != NULL){
		n = ib->start;
		ib->start = ib->start->next;
		n->next = NULL;

		if( ib->start == NULL) ib->end = NULL;
	}

	return n;
}

void freeItems(Item *s){
	while (s != NULL){
		Item *aux = s->next;
		free(s);
		s = aux;
	}
}

Monkey *createMonkey(unsigned id, ItemBuffer items, char op, unsigned op_num,
		unsigned test_num, unsigned if_true, unsigned if_false){
	Monkey *m = (Monkey *)malloc(sizeof(Monkey));

	if (m != NULL ){
		m->id = id;
		m->items = items;
		m->op = op;
		m->op_num = op_num;
		m->test_num = test_num;
		m->if_true =if_true;
		m->if_false =if_false;
		m->inspecs = 0;
		m->next = NULL;
	}
	else {
		perror("createMonkey");
		exit(EXIT_FAILURE);
	}

	return m;
}

Monkey *addToMonkeys(Monkey *s, Monkey *n){
	Monkey *p = s;
	n->next = NULL;

	if (p != NULL){
		while (p->next != NULL) p = p->next;
		p->next = n;
	}
	else {
		s = n;
	}

	return s;
}

void freeMonkeys(Monkey *s){
	while (s != NULL){
		Monkey *aux = s->next;
		freeItems(s->items.start);
		free(s);
		s = aux;
	}
}

void getMonkeyNum(FILE *fp, char *sf, unsigned *n){
	char line[MAX_LINE];

	if (fgets(line, MAX_LINE, fp) == NULL){
		perror("getMonkeyNum");
		exit(EXIT_FAILURE);
	}

	if (sscanf(line, sf, n) != 1){
		errno = ENODATA;
		perror("getMonkeyNum");
		exit(EXIT_FAILURE);
	}
}

void getMonkeyItems(FILE *fp, ItemBuffer *ib){
	char c = fgetc(fp);

	while (c != ':' && c != '\n' && !feof(fp)) c = fgetc(fp);

	if (c != ':'){
		errno = ENODATA;
		perror("getMonkeyItems");
		exit(EXIT_FAILURE);
	}
	else {
		c = fgetc(fp);
		while (c != '\n' && !feof(fp)){
			char num[MAX_LINE];
			unsigned i = 0;
			unsigned n;

			while (c != ',' && c != '\n'){
				num[i++] = c;
				c = fgetc(fp);
			}

			num[i] = '\0';

			if (sscanf(num, "%u", &n) != 1){
				errno = ENODATA;
				perror("getMonkeyItems");
				exit(EXIT_FAILURE);
			}

			Item *item = createItem(n);
			addToItems(ib, item);

			if (c != '\n') c = fgetc(fp);
		}

		if (feof(fp)){
			errno = ENODATA;
			perror("getMonkeyItems");
			exit(EXIT_FAILURE);
		}
	}
}

void getMonkeyOp(FILE *fp, char *m_op, unsigned *m_op_num){
	char line[MAX_LINE];

	if (fgets(line, MAX_LINE, fp) == NULL){
		perror("getMonkeyOp");
		exit(EXIT_FAILURE);
	}

	if (sscanf(line, "  Operation: new = old %c %u\n",
				m_op, m_op_num) == 2){
	}
	else if (sscanf(line, "  Operation: new = old %c old\n", m_op) == 1){
		*m_op_num = 0;
	}
	else {
		errno = ENODATA;
		perror("getMonkeyOp");
		exit(EXIT_FAILURE);
	}
}

Monkey *readInput(void){
	Monkey *m = NULL;
	FILE *fp = fopen("input.txt", "r");

	if (fp != NULL){
		while(!feof(fp)){
			char m_op, c;
			unsigned m_id, m_op_num, m_test, m_true, m_false;
			ItemBuffer m_ib = {NULL, NULL};
			Monkey *newM;

			getMonkeyNum(fp, "Monkey %u:\n", &m_id);
			getMonkeyItems(fp, &m_ib);
			getMonkeyOp(fp, &m_op, &m_op_num);
			getMonkeyNum(fp, "  Test: divisible by %u\n", &m_test);
			getMonkeyNum(fp, "    If true: throw to monkey %u\n",
					&m_true);
			getMonkeyNum(fp,  "    If false: throw to monkey %u\n",
					&m_false);
			while ((c =fgetc(fp)) != '\n' && !feof(fp)); // Ignore empty line until EOF

			newM = createMonkey(m_id, m_ib, m_op, m_op_num,
					m_test, m_true, m_false);
			m = addToMonkeys(m, newM);
		}

		fclose(fp);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	return m;
}

void monkeyTurn(Monkey *s, Monkey *m, unsigned cDiv){
	Item *i = removeFromItems(&(m->items));

	while (i != NULL){
		unsigned id;

		i->worry = calcWorry(m->op, m->op_num, i->worry, cDiv);

		id = calcThrow(m->test_num, m->if_true, m->if_false, i->worry);
		throwItem(s, id, i);

		m->inspecs++;
		i = removeFromItems(&(m->items));
	}
}

unsigned calcWorry(char op, unsigned op_num, long unsigned i, unsigned cDiv){
	if (op == '+'){
		i += op_num;
	}
	else if (op == '*' && op_num != 0){
		i *= op_num;
	}
	else {
		i *= i;
	}

	return i % cDiv;
}

unsigned calcThrow(unsigned test, unsigned if_true, unsigned if_false,
		long unsigned i){
	if (i % test == 0){
		return if_true;
	}
	else {
		return if_false;
	}
}

void throwItem(Monkey *s, unsigned id, Item *i){
	Monkey *ptr = s;

	while (ptr != NULL && ptr->id != id) ptr = ptr->next;

	if (ptr == NULL){
		errno =  EFAULT;
		perror("throwItem");
		exit(EXIT_FAILURE);
	}

	addToItems(&ptr->items, i);
}

Monkey *monkeySortIns(Monkey *s){
	Monkey *i = s;

	if (i != NULL){
		Monkey *previ = NULL;

		while (i->next != NULL) {
			Monkey *max = i, *prevmax = previ;
			Monkey *j = i->next, *prevj = i;

			while (j != NULL){
				if (max->inspecs < j->inspecs){
					prevmax = prevj;
					max = j;
				}

				prevj = j;
				j = j->next;
			}

			if (i != max){
				if (previ != NULL){
					previ->next = max;
				}
				else {
					s = max;
				}
				prevmax->next = max->next;
				max->next = i;

				previ = max;
			}
			else {
				previ = i;
				i = i->next;
			}
		}
	}

	return s;
}

int main(void){
	Monkey *s = readInput();
	Monkey *m = s;

	unsigned cDiv = 1;
	while (m != NULL){
		cDiv *= m->test_num;
		m = m->next;
	}

	for (unsigned i = 0; i < 10000; i++){
		m = s;

		while (m != NULL){
			monkeyTurn(s, m, cDiv);
			m = m->next;
		}
	}

	s = monkeySortIns(s);

	printf("Result: %lu\n", (long unsigned)s->inspecs * (long unsigned)s->next->inspecs);

	freeMonkeys(s);

	return 0;
}
