#!/usr/bin/env python3

SZ_MAX = 100000

class Folder:
    def __init__(self):
        self.size = 0
        self.parent = None
        self.children = []

    def addChild(self, child):
        self.children.append(child)
        child.parent = self

class Total:
    def __init__(self):
        self.size = 0

    def addSize(self, a):
        self.size += a


def readInput(f, root):
    line = f.readline()
    line = f.readline()
    
    pwd = root

    while line != "":
        fields = line.split()

        if fields[0] == "$" and fields[1] == "cd":
            if fields[2] == "..":
                pwd = pwd.parent

            else:
                nf = Folder()
                pwd.addChild(nf)
                pwd = nf

        elif fields[0].isdigit():
            pwd.size += int(fields[0])

        line = f.readline()

def sumFolders(pwd, total):
    suma = pwd.size

    for i in pwd.children:
        suma += sumFolders(i, total)

    if suma <= SZ_MAX:
        total.addSize(suma)

    return suma

def main():
    with open("input.txt", "rt") as f:
        root = Folder()
        readInput(f, root)
        total = Total()
        sumFolders(root, total)
        print("Sum of small folders: " + str(total.size))

if __name__ == "__main__":
    main()
