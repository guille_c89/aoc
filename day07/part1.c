#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#define NAME_MAX 15
#define LINE_MAX 20
#define SZ_MAX 100000

typedef struct Folder {
	unsigned long size;
	struct Folder *p;
	struct Folder *n;
	struct Folder *hch;
} Folder;

Folder *newFolder(void);
void addChildren(Folder *parent, Folder *child);
unsigned long sumFolders(Folder *pwd, unsigned long *s);
void readInput(FILE *fp, Folder *root);
void delFolder(Folder *pwd);

int main(void){
	FILE *fp = fopen("input.txt", "r");

	if ( fp != NULL){
		Folder *root = newFolder();

		readInput(fp, root);

		unsigned long sum = 0;
		sumFolders(root, &sum);
		printf("Sum of small folders: %lu\n", sum);

		delFolder(root);
	}
	else {
		perror("fopen");
		exit(EXIT_FAILURE);
	}


	exit(EXIT_SUCCESS);
}

Folder *newFolder(void){
	Folder *f = malloc(sizeof(Folder));

	if (f != NULL){
		f->size = 0;
		f->p = NULL;
		f->n = NULL;
		f->hch = NULL;
		return f;
	}
	else {
		fprintf(stderr, "%s", "newFolder: can't assign menory");
		exit(EXIT_FAILURE);
	}

}

void addChildren(Folder *parent, Folder *child){
	child->p = parent;
	child->n = parent->hch;
	parent->hch = child;
}

void readInput(FILE *fp, Folder *root){
	Folder *pwd = root; // Init on root
	char line[LINE_MAX];

	fgets(line, LINE_MAX, fp); // Ignores first cd
	fgets(line, LINE_MAX, fp);

	while (!feof(fp)){

		if (strncmp("$ cd ", line, 5) == 0){
			if (strcmp("..\n", &line[5]) == 0){
				pwd = pwd->p;
			}
			else {
				Folder *nf = newFolder();
				addChildren(pwd, nf);
				pwd = nf;
			}
		}
		else if (isdigit(line[0])){
			char *endPtr;
			unsigned long bytes;

			errno = 0;
			bytes = strtoul(line, &endPtr, 10);

			if (errno != 0){
				perror("strtoul");
				exit(EXIT_FAILURE);
			}

			if (endPtr != line){
				pwd->size += bytes;
			}
		}

		fgets(line, LINE_MAX, fp);
	}

	fclose(fp);
}

unsigned long sumFolders(Folder *pwd, unsigned long *s){
	unsigned long sum = pwd->size;
	Folder *child = pwd->hch;

	while (child != NULL){
		sum += sumFolders(child, s);
		child = child->n;
	}

	if (sum <= SZ_MAX){
		*s += sum;
	}

	return sum;
}

void delFolder(Folder *pwd){

	while (pwd->hch != NULL){
		Folder *tmp = pwd->hch->n;
		delFolder(pwd->hch);
		pwd->hch = tmp;
	}

	free(pwd);
}
