# Day 7: No Space Left On Device

[[_TOC_]]

## Part 1

You can hear birds chirping and raindrops hitting leaves as the expedition
proceeds. Occasionally, you can even hear much louder sounds in the distance;
how big do the animals get out here, anyway?

The device the Elves gave you has problems with more than just its
communication system. You try to run a system update:

```bash
$ system-update --please --pretty-please-with-sugar-on-top
Error: No space left on device
```

Perhaps you can delete some files to make space for the update?

You browse around the filesystem to assess the situation and save the resulting
terminal output (your puzzle input). For example:

```bash
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
```

The filesystem consists of a tree of files (plain data) and directories (which
can contain other directories or files). The outermost directory is called /.
You can navigate around the filesystem, moving into or out of directories and
listing the contents of the directory you're currently in.

Within the terminal output, lines that begin with $ are commands you executed, very much like some modern computers:

- cd means change directory. This changes which directory is the current
  directory, but the specific result depends on the argument:
  - cd x moves in one level: it looks in the current directory for the
    directory named x and makes it the current directory.
  - cd .. moves out one level: it finds the directory that contains the current
    directory, then makes that directory the current directory.
  - cd / switches the current directory to the outermost directory, /.
- ls means list. It prints out all of the files and directories immediately
  contained by the current directory:
  - 123 abc means that the current directory contains a file named abc with
    size 123.
  - dir xyz means that the current directory contains a directory named xyz.

Given the commands and output in the example above, you can determine that the
filesystem looks visually like this:

- / (dir)
  - a (dir)
    - e (dir)
      - i (file, size=584)
    - f (file, size=29116)
    - g (file, size=2557)
    - h.lst (file, size=62596)
  - b.txt (file, size=14848514)
  - c.dat (file, size=8504156)
  - d (dir)
    - j (file, size=4060174)
    - d.log (file, size=8033020)
    - d.ext (file, size=5626152)
    - k (file, size=7214296)

Here, there are four directories: / (the outermost directory), a and d (which
are in /), and e (which is in a). These directories also contain files of
various sizes.

Since the disk is full, your first step should probably be to find directories
that are good candidates for deletion. To do this, you need to determine the
total size of each directory. The total size of a directory is the sum of the
sizes of the files it contains, directly or indirectly. (Directories themselves
do not count as having any intrinsic size.)

The total sizes of the directories above can be found as follows:

- The total size of directory e is 584 because it contains a single file i of
  size 584 and no other directories.
- The directory a has total size 94853 because it contains files f (size
  29116), g (size 2557), and h.lst (size 62596), plus file i indirectly (a
  contains e which contains i).
- Directory d has total size 24933642.
- As the outermost directory, / contains every file. Its total size is
  48381165, the sum of the size of every file.

To begin, find all of the directories with a total size of at most 100000, then
calculate the sum of their total sizes. In the example above, these directories
are a and e; the sum of their total sizes is 95437 (94853 + 584). (As in this
example, this process can count files more than once!)

Find all of the directories with a total size of at most 100000. What is the
sum of the total sizes of those directories?

### Refinement 1

Load file system usage distribution on a "tree". Each node is a folder
containing name of folder, direct size (files in same folder) a pointer to
parent (`NULL` for `/` root), a pointer for next folder on same level (linked
list) and pointer to _head_ of subfolders (`NULL` for deeper level/leaf).

### Refinement 2

#### Data

##### Symbolic Constant: `NAME_MAX`

Max length of the name of a folder: `15`

##### Symbolic Constant: `LINE_MAX`

Max length of a line: `15`

##### Symbolic Constant: `SZ_MAX`

Max length of a line: `100000`

##### Structure `Folder`

- `name`: array of characters of size `NAME_MAX` with name of folder.
- `size`: total bytes of files in the folder (subfolders not included).
- `p`: pointer to parent folder (`NULL` for root `/`).
- `n`: pointer to next folder.
- `hch`: head on children list.

#### Function `newFolder`

Creates a folder's node with provide name, zero space usage and without
connections.

##### Inputs

- `n`: folder name.

##### Output

Pointer to `Folder` created.

##### Pseudo-code

1. Invoke `malloc()` for RAM assignment to `Forder` pointer `f`.
1. If `f` is different of `NULL`:
   1. Copy `n` to `name` member.
   1. Initialize `size` to `0`.
   1. Initialize pointer to parent `p` to `NULL`.
   1. Initialize pointer to next brother `n` to `NULL`.
   1. Initialize pointer to header of children `hch` to `NULL`.
   1. Return `f`.
1. Else:
   1. Print error.
   1. Terminate program execution.

#### Function `addChildren`

Set relation between a parent folder and a child folder.

##### Inputs

- `parent`: pointer to folder.
- `child`: pointer to folder.

##### Output

Void.

##### Pseudo-code

1. Assign `parent` pointer to `p` member of `child`.
1. Assign `hch` of `parent` to member `n` of `child`.
1. Assign `child` pointer to `hch` of parent.


#### Function `sumFolders`

Walk over folder tree and sum folders with less than `SZ_MAX` (child folders
included).

##### Inputs

- `pwd`: pointer to folder.
- `s`: pointer to "grand total".

##### Output

Total size of folder.

##### Pseudo-code

1. Declare `sum` and assign member `size` of pointer `pwd`.
1. Declare pointer `child` and assign member `hch` of `pwd`.
1. While `child` is different of `NULL`:
   1. Invoke `sumFolders()` for `child` and `s` and add return to  `sum`.
   1. Assign member `n` of `child` to  `child`.
1. If sum is less or equal than `SZ_MAX`:
   1. Add `sum` to `s`.
1. Return `sum`.

#### Function `readInput`

Walk over folder tree on input file and creates each folder with size and
"connections" (parent folder and subfolders).

##### Inputs

- `fd`: pointer to FILE.
- `root`: pointer to `/` folder.

##### Output

Void.

##### Pseudo-code

1. Copy pointer of `root` to `pwd` (current folder).
1. Declare string `line` of size `LINE_MAX`.
1. Read two lines (skip `cd /`).
1. While is not `EOF`:
   1. If first `5` bytes of line equals to `"$ cd "`:
      1. If after `5` postions `line` equals to `"..\n"`:
         1. Set `pwd` as current parent `pwd->p`.
      1. Else:
         1. Invoke `newFolder()` with name `line[5]` and assign to pointer
            `nf`.
         1. Invoke `addChildren()` for `pwd` as parent and `nf` as child.
         1. Assign `nf` to `pwd`.
   1. Else if first character of line is a digit:
      1. Declare pointer `endPtr` (number conversion detection).
      1. Declare `bytes` (local save of size).
      1. Reset error number to `0`.
      1. Invoke `strtoul()` to `line`, `endPtr` and base `10`, assign return
         to `bytes`.
      1. If error ocurred:
         1. Print error.
         1. Terminate program execution.
      1. If conversión succesull:
         1. Add `bytes` to member `size` of `pwd`.
   1. Get a new line.
1. Close file `fp`.

#### Function `delFolder`

Walk over folder tree and remove folders.

##### Inputs

- `pwd`: pointer to current folder.

##### Output

Void.

##### Pseudo-code

1. While head of children `hch` of `pwd` if different of `NULL`:
   1. Declare folder `tmp` and assign pointer to next child on linked list.
   1. Invoke `delFolder()` for actual head.
   1. Swap head to next on list.
1. Invoke `free()` for current folder `pwd`.

#### Function `main`

1. Open file and assign to pointer `fp`.
1. If file opened successfully:
   1. Create `root` folder invoking `newFolder()` with name `"/\n"`.
   1. Invoke `readInput()` for file `fp` and folder `root`.
   1. Declare `sum` and assign `0`.
   1. Invoke `sumFolders()` for `root` and `sum`.
   1. Print result.
   1. Invoke `delFolder()` for `root`.
1. Else: 
   1. Print error.
   1. Terminate program execution.

## Part 2

Now, you're ready to choose a directory to delete.

The total disk space available to the filesystem is 70000000. To run the
update, you need unused space of at least 30000000. You need to find a
directory you can delete that will free up enough space to run the update.

In the example above, the total size of the outermost directory (and thus the
total amount of used space) is 48381165; this means that the size of the unused
space must currently be 21618835, which isn't quite the 30000000 required by
the update. Therefore, the update still requires a directory with total size of
at least 8381165 to be deleted before it can run.

To achieve this, you have the following options:

- Delete directory e, which would increase unused space by 584.
- Delete directory a, which would increase unused space by 94853.
- Delete directory d, which would increase unused space by 24933642.
- Delete directory /, which would increase unused space by 48381165.

Directories e and a are both too small; deleting them would not free up enough
space. However, directories d and / are both big enough! Between these, choose
the smallest: d, increasing unused space by 24933642.

Find the smallest directory that, if deleted, would free up enough space on the
filesystem to run the update. What is the total size of that directory?
