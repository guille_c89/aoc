#!/usr/bin/env python3

DISK_CAP = 70000000
NEED_SPACE = 30000000

class Folder:
    def __init__(self):
        self.size = 0
        self.parent = None
        self.children = []

    def addChild(self, child):
        self.children.append(child)
        child.parent = self

class Diff:
    def __init__(self):
        self.d = NEED_SPACE

    def setDiff(self, a):
        self.d = a

def readInput(f, root):
    line = f.readline()
    line = f.readline()
    
    pwd = root

    while line != "":
        fields = line.split()

        if fields[0] == "$" and fields[1] == "cd":
            if fields[2] == "..":
                pwd = pwd.parent

            else:
                nf = Folder()
                pwd.addChild(nf)
                pwd = nf

        elif fields[0].isdigit():
            pwd.size += int(fields[0])

        line = f.readline()

def sumFolders(pwd):
    suma = pwd.size

    for i in pwd.children:
        suma += sumFolders(i)

    return suma

def findDiff(pwd, rm, diff):
    suma = pwd.size

    for i in pwd.children:
        suma += findDiff(i, rm, diff)

    if suma >= rm and suma - rm < diff.d:
        diff.setDiff(suma - rm)

    return suma

def main():
    with open("input.txt", "rt") as f:
        root = Folder()
        readInput(f, root)
        rm = NEED_SPACE - (DISK_CAP - sumFolders(root))
        diff = Diff()
        findDiff(root, rm, diff)
        print("Sum of small folders: " + str(diff.d + rm))

if __name__ == "__main__":
    main()
